Steps followed to install DCOS on AWS

https://dcos.io/docs/1.9/installing/cloud/aws/basic/ ==>
https://downloads.dcos.io/dcos/EarlyAccess/aws.html?_ga=2.132111437.311503692.1499848807-1195962584.1499848807

Cloudformation Template
https://s3-us-west-2.amazonaws.com/downloads.dcos.io/dcos/EarlyAccess/commit/0ce03387884523f02624d3fb56c7fbe2e06e181b/cloudformation/single-master.cloudformation.json

myip: 188.94.68.194/32

On completion of stack creation, Copy the Master URL from the outputs and open in the browser.
Login

Install CLI from the drop down in the Left pane

SSH to nodes
-------------

Refer: https://dcos.io/docs/1.8/administration/access-node/sshcluster/

Login and generate the TLS certificate and key in the master node of mesos
----------------------------------------------------------------------------

dcos node ssh --master-proxy --leader
mkdir -p ~/genconf/serve; cd ~/genconf/serve
openssl req -newkey rsa:4096 -nodes -sha256 -keyout domain.key -x509 -days 365 -out domain.crt

Scp the pem file to master
----------------------------
master=34.251.160.86
key=~/.ssh/ariya_hdf_hdp_ire.pem
key=~/.ssh/temp_ire.pem
scp -i $key $key core@$master:~/

ssh -i $key core@$master

dcos node ssh --master-proxy --leader





Distribute the docker.tar across all the machines
--------------------------------------------------
docker login
sudo chown root:core  ~/.docker/config.json
sudo chown root:core  ~/.docker

sudo tar czf docker.tar.gz .docker
sudo tar -tvf ~/docker.tar.gz

MESOS_AGENTS=$(curl -sS master.mesos:5050/slaves | jq '.slaves[] | .hostname' | tr -d '"');
for i in $MESOS_AGENTS; do scp -o StrictHostKeyChecking=no docker.tar.gz "$i":~/; done
for i in $MESOS_AGENTS; do ssh "$i" -oStrictHostKeyChecking=no "sudo mv docker.tar.gz /etc/"; done

for i in $MESOS_AGENTS; do ssh "$i" -oStrictHostKeyChecking=no "sudo rm /etc/docker.tar.gz"; done

# https://dcos.io/docs/1.8/administration/access-node/tunnel/

 brew install openvpn
dcos package install tunnel-cli --cli
dcos tunnel socks

 curl --proxy socks5h://127.0.0.1:1080 ariya-eureka.marathon.agentip.dcos.thisdcos.directory:8761


## Whether you use portMappings or portDefinitions depends on whether you are using BRIDGE or HOST networking




================================================================================================================================================================

Machines has coreos. Install below to enable some packages to be used
-----------------------------------------------------------------------

key=~/.ssh/ariya_hdf_hdp_ire.pem
key=~/temp_ire.pem


eval `ssh-agent -s` && ssh-add $key


#In core user, not in root(fedora)
------------------------------------
cd genconf/serve/

MESOS_AGENTS=$(curl -sS master.mesos:5050/slaves | jq '.slaves[] | .hostname' | tr -d '"');
for i in $MESOS_AGENTS; do ssh "$i" -oStrictHostKeyChecking=no "sudo sysctl -w net.netfilter.nf_conntrack_tcp_be_liberal=1"; done
for i in $MESOS_AGENTS; do ssh "$i" -oStrictHostKeyChecking=no "sudo mkdir --parent /etc/privateregistry/certs/"; done
for i in $MESOS_AGENTS; do scp -o StrictHostKeyChecking=no ./domain.* "$i":~/; done
for i in $MESOS_AGENTS; do ssh "$i" -oStrictHostKeyChecking=no "sudo mv ./domain.* /etc/privateregistry/certs/"; done
for i in $MESOS_AGENTS; do ssh "$i" -oStrictHostKeyChecking=no "sudo mkdir --parent /etc/docker/certs.d/registry.marathon.l4lb.thisdcos.directory:5000"; done
for i in $MESOS_AGENTS; do ssh "$i" -oStrictHostKeyChecking=no "sudo cp /etc/privateregistry/certs/domain.crt /etc/docker/certs.d/registry.marathon.l4lb.thisdcos.directory:5000/ca.crt"; done
for i in $MESOS_AGENTS; do ssh "$i" -oStrictHostKeyChecking=no "sudo systemctl restart docker"; done

Install Registry from Universe using Advanced Settings inside Security tab

Test:
-----
docker pull nginx
docker tag nginx registry.marathon.l4lb.thisdcos.directory:5000/nginx

docker pull comptelhki/ngap_workflow-service:3.0.1
docker pull comptelhki/ngap_ui:3.0.1
docker pull comptelhki/ngap_spark-service:3.0.1
docker pull postgres:8.4
docker pull comptelhki/ngap_configserver:3.0.1
docker pull comptelhki/ngap_eureka:3.0.1

Env:
----
dcos package install marathon-lb
https://www.accenture.com/us-en/blogs/blogs-docker-dependency-using-mesospheres-marathon-groups




Extras:
--------

/usr/bin/toolbox - Logins to root and uses fedora OS --?!! Just do it :-)
yum install nano
yum install sudo
sudo yum install -y jq
exit


Convert existing docker compose yaml file to Marathon application file
------------------------------------------------------------------------

docker pull micahhausler/container-transform:latest

cd src/main/infra/dcos/
cat docker-compose.yml | docker run --rm -i micahhausler/container-transform -o marathon > docker-compose.json
