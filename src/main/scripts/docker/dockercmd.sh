#!/usr/bin/env bash

docker ps
docker-compose -f $file.yml -p $profile_name up
docker-compose -f $file.yml -p $profile_name up -d ==> Restart only the changed services
docker exec -it docker_container_name bash ==> Login to the docker container
service docker stop
service docker start

# Update docker-compose file which had been already running the instances
# This will run the modifications done on the docker compose yaml file
docker-compose -f $file.yml -p $profile_name up -d
