# This file gives a detailed explanation of the files and templates to be created as part of default/customer based FAA installation
# It comprises of following section

    # Files that has to be placed in Edge node
    # Files that has to be modified for a customer installation
    # Scripts that help in installation
    # Scripts that help in post installation
    # Files and scripts that has to be executed at the Hadoop cluster

Cloud Installation of NGAP+FAA:
-------------------------------
Components Needed:
------------------

CloudFormation File - cloudinstallation/faacloudformation.json

    File with end to end information on creating the infrastructure and do the

    Prerequisite:
    --------------
    AWS CLI installed on the Edge node, configured with the appropriate AWS credentials
    Edge node is the node which is used to execute the cloudformation script
    Hadoop cluster up and running inside a VPC and Subnet

    Parameters:
    ------------
    Update the below parameters inside the cloudformation file

        - $$master_public_ip ==> public ip of the master node of Hadoop cluster
        - $$master_priv_ip   ==> private ip of the master node of Hadoop cluster
        - $$master_user      ==> login user of the master node of Hadoop cluster
        - $$master_pwd       ==> login password of the master node of Hadoop cluster
        - $$docker_user      ==> docker user to private docker hub
        - $$docker_pwd       ==> docker password to private docker hub
        - $$customer         ==> customer name for whom the installation is being done

    Run Command: with params
    -------------------------

    aws cloudformation create-stack --stack-name $$stack_name --template-body $$cloudformation_jsonfilepath --parameters ParameterKey=KeyName,ParameterValue=$$aws_pem_filename ParameterKey=InstanceName,ParameterValue=$$instance_name ParameterKey=SubnetId,ParameterValue=$$subnetid ParameterKey=SnapshotId,ParameterValue=$$snapshot_id

    Params:
    -------

        - $$stack_name - Arbitrary name for the stack to be created
        - $$cloudformation_jsonfilepath - File path of the cloudformation json file - prepend path with file:///
        - $$aws_pem_filename - AWS pem file to be used to create the stack - Do not include '.pem'
        - $$instance_name - Arbitrary name of the instance
        - $$subnetid - Subnet Id of the
        - $$snapshot_id - Snapshot Id from which an EBS volume to be created and mounted

    Output:
    -------

    FAA Stack with $$stack_name will be launched in the AWS cloud formation page
    On success of the stack creation,
        FAA host machine with $$instance_name will be spun up with the ngap and faa installation done and dockers up and running


    Post Installation:
    -------------------

    Execute below commands in the hadoop cluster
    Move the scripts from edgenode/createuser.sh and edgenode/restart.sh scripts to the Hadoop cluster
        - sh createuser.sh $$master_user
            - Will be prompted for password and type in $$master_pwd
        - sh restart.sh
    Exit and relogin with the new user created on the Master node of hadoop
    Execute the commands in the below scripts
        - toMasterNode/executeOnMaster.sh
        - sh postinst/moveFilesFromMaster.sh
        - sh postinst/moveFilesToMaster.sh


Preparation of the Snapshot:
-----------------------------

Snapshot is just an image of the EBS volume which has the files needed for the installation
Launch an EC2
Create and Attach an EBS volume to the EC2
Mount the volume on the EC2 machine under /image
Copy the files in faahost/* to /image/installations
Create snapshot of the volume and note down the snapshot id

CF Trigger:
-------------
aws cloudformation create-stack --capabilities CAPABILITY_IAM --stack-name ariya-dev-1 --template-body file:///Users/ariyabalasadaiappan/repos/ngap/aws/FAA_CloudFormation_dev.json --parameters ParameterKey=InstanceName,ParameterValue=ariya-dev-1 ParameterKey=KeyName,ParameterValue=ariya_hdf_hdp_ire ParameterKey=SubnetId,ParameterValue=subnet-20178b47 ParameterKey=DockerLogin,ParameterValue=UUU ParameterKey=DockerPassword,ParameterValue=PPP

aws cloudformation create-stack --capabilities CAPABILITY_IAM --stack-name ariya-ha-1 --template-body file:///Users/ariyabalasadaiappan/repos/ngap/aws/FAA_CloudFormation_HA.json --parameters ParameterKey=InstanceName,ParameterValue=ariya-ha-1 ParameterKey=KeyName,ParameterValue=ariya_hdf_hdp_ire ParameterKey=SubnetId,ParameterValue=subnet-20178b47 ParameterKey=DockerLogin,ParameterValue=UUU ParameterKey=DockerPassword,ParameterValue=PPP ParameterKey=AvailabilityZone,ParameterValue=eu-west-1a