#!/usr/bin/env bash
if [ $# -eq 0 ]
   
 then 
 
    echo "No arguments supplied" 
 
  exit 
fi 
export user=$1 
echo "Creating $user" 
sudo useradd -s /bin/bash -m -d /home/$user -g root $user   
echo "$user ALL=(ALL:ALL) ALL" | sudo tee --append /etc/sudoers 
sudo passwd $user 
