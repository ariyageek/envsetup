#!/usr/bin/env bash
echo "Start of FAA Installation"

sh $inst_dir/ngap-scripts/prepareFAAProperties.sh $master_user $master_pwd $faa_priv_ip $faa_public_ip $master_priv_ip $inst_dir/templates/import_template.properties
docker run -i --net $customer'_default' --env-file $inst_dir/templates/import.properties comptelhki/faa-installer

echo "End:: FAA Installation"

echo "Copying jars from docker to host machine"
docker cp $customer'_workflow-service_1':/ngap/jars $inst_dir/toMasterNode/


