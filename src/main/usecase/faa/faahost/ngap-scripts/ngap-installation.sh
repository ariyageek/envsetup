#!/usr/bin/env bash

# Docker compose installation

curl -L "https://github.com/docker/compose/releases/download/1.11.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/bin/docker-compose
chmod +x /usr/bin/docker-compose

service docker stop
sleep 10
service docker start

echo "****** Start of NGAP Installation *****"
docker-compose -f /home/$user/ngap/docker-compose.yml -p $customer up -d
echo "****** End of NGAP Installation *****"

docker ps
sh /image/installations/ngap-scripts/ui-start-detect.sh