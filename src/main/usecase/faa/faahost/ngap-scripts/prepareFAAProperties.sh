#!/usr/bin/env bash
user=$1
pwd=$2
faa_host_priv_ip=$3 
faa_host_public_ip=$4
master_priv_ip=$5
import_template=$6

postgre_port=$(docker ps | grep postgres | awk '{print $10;}' | cut -d':' -f 2 | cut -d'-' -f 1)

template_dir=$(dirname $import_template)
import_file=$template_dir/import.properties

cp $import_template $import_file

if [ $# -ne 6 ] 
 then 
    echo "Usage: sh prepareProperties.sh user pwd faa_host_priv_ip faa_host_public_ip master_priv_ip import_template_file_path" 
    echo "Ex: sh prepareProperties.sh ngap ngap 10.0.1.60 10.0.1.212 /data/installations/help-scripts/import_template.properties" 
  exit 
fi 

postgre_url="jdbc:postgresql:\/\/$faa_host_priv_ip:$postgre_port\/fds?user=postgres\&amp;password=postgres\&amp;searchpath=fdaobjectstore"
hdfsbasepath="hdfs:\/\/\/user\/$user"
xmlexportpath="\/home\/$user\/data\/xml_export"

sed -i -e 's/\${fds.url}\$/'"$postgre_url"'/g' $import_file

sed -i -e 's/\${ngap.data.output.dir}\$/'"$hdfsbasepath\/data"'/g' $import_file
sed -i -e 's/\${xml.output.dir}\$/'"$xmlexportpath"'/g' $import_file

sed -i -e 's/\${ui.url}\$/'"$faa_host_priv_ip:8080"'/g' $import_file

sed -i -e 's/\${master.host}\$/'"$master_priv_ip"'/g' $import_file
sed -i -e 's/\${master.username}\$/'"$user"'/g' $import_file
sed -i -e 's/\${master.password}\$/'"$pwd"'/g' $import_file

echo "Prepared properties file : $import_file"
