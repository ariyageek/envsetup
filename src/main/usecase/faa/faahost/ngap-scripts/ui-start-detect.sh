#!/usr/bin/env bash
sudo chown $user:root /home/$user/ngap

docker ps | grep eureka | cut -d":" -f 3 | cut -d"-" -f 1 > /home/$user/ngap/eureka-port.txt

export EUREKA_PORT=$(cat /home/$user/ngap/eureka-port.txt)

echo "Hitting http://localhost:${EUREKA_PORT}/eureka/apps/UI"
status=$(curl -v -X "GET" http://localhost:${EUREKA_PORT}/eureka/apps/UI | grep "<status>")
while [ true ]
do
 status=$(curl -v -X "GET" http://localhost:${EUREKA_PORT}/eureka/apps/UI | grep "<status>")
 sleep 10
 if [ $status = "<status>UP</status>" ]
 then
  echo "UI Status: $status"
  break
 fi
done

echo "Adding the license file"

# Do not remove the sleep as the eureka status for UI is UP doesnt immediately open the UI port
echo "Sleeping for 120s"
sleep 120

docker ps | grep ui | cut -d":" -f 3 | cut -d"-" -f 1 > /home/$user/ngap/ui-port.txt
export UI_PORT=$(cat /home/$user/ngap/ui-port.txt)

echo "Hitting http://localhost:${UI_PORT}/api/license"
curl -X POST -H "login: comptel" -H "password: c0mpte1" http://localhost:${UI_PORT}/api/license -F "licenseFile=@$inst_dir/keys/license.key"
