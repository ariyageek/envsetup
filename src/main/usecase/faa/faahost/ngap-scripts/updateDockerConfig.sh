#!/usr/bin/env bash

service docker stop

docker_conf_file=/etc/sysconfig/docker
docker_conf_file_bkup=/etc/sysconfig/docker.bak


cp $docker_conf_file $docker_conf_file_bkup
new=$(cat $docker_conf_file|grep OPTIONS|grep -v "#" | sed 's/\"//g' | awk '{print $1 $2 " -g /image/docker -p /var/run/docker.pid\""}'| sed "s/OPTIONS=/OPTIONS=\"/g")
line=$(grep -nr OPTIONS $docker_conf_file | grep -v "#" | cut -d ':' -f1)

echo $new

awk -v no="$line" 'NR!=no' $docker_conf_file_bkup > $docker_conf_file
sed -i "$((line))i\ $new" $docker_conf_file

echo "Updated the $docker_conf_file" 
cat $docker_conf_file

service docker start

docker info
